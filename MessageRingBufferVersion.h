// Library version in HEX: 0xMMmmpp (MM=major, mm=minor, pp=patch)
// The version is defined in this format because:
// - the expression can be evaluated by the C Preprocessor
// - bumpversion can handle it
#define MessageRingBuffer_VERSION (1 * 65536 + 1 * 256 + 2)

const uint8_t MessageRingBuffer_version_major =
  MessageRingBuffer_VERSION >> 16;
const uint8_t MessageRingBuffer_version_minor =
  MessageRingBuffer_VERSION >> 8 & 0xFF;
const uint8_t MessageRingBuffer_version_patch =
  MessageRingBuffer_VERSION & 0xFF;
