/*

  Debug macros

*/
#define MRBDF(s)                                                              \
  {                                                                           \
    Serial.print(F(s));                                                       \
  };
#define MRBDP(s)                                                              \
  {                                                                           \
    Serial.print(s);                                                          \
  };
#define MRBDH(s)                                                              \
  {                                                                           \
    MRBDF("0x");                                                              \
    Serial.print(s, HEX);                                                     \
  };
#define MRBDh(s)                                                              \
  {                                                                           \
    Serial.print(s, HEX);                                                     \
  };
#define MRBDL()                                                               \
  {                                                                           \
    Serial.println();                                                         \
  };

#ifdef MessageRingBuffer_DEBUG
#define MRBDBG(cmd)                                                           \
  {                                                                           \
    MRBDF("MeRiBu: ");                                                        \
    cmd;                                                                      \
    MRBDL();                                                                  \
    Serial.flush();                                                           \
  };
#else
#define MRBDBG(cmd)
#endif

#define MRBDS(s) MRBDBG(MRBDF(s))
#define MRBDSN(s, n) MRBDBG(MRBDF(s); MRBDP(n))
#define MRBDSNS(s1, n, s2) MRBDBG(MRBDF(s1); MRBDP(n); MRBDF(s2))
#define MRBDSNSN(s1, n1, s2, n2)                                              \
  MRBDBG(MRBDF(s1); MRBDP(n1); MRBDF(s2); MRBDP(n2))
#define MRBDSNSNS(s1, n1, s2, n2, s3)                                         \
  MRBDBG(MRBDF(s1); MRBDP(n1); MRBDF(s2); MRBDP(n2); MRBDF(s3))
#define MRBDSNSNSH(s1, n1, s2, n2, s3, n3)                                    \
  MRBDBG(MRBDF(s1); MRBDP(n1); MRBDF(s2); MRBDP(n2); MRBDF(s3); MRBDH(n3))
#define MRBDSH(s, n) MRBDBG(MRBDF(s); MRBDH(n))
#define MRBDSHS(s1, n, s2) MRBDBG(MRBDF(s1); MRBDH(n); MRBDF(s2))
#define MRBDSHSH(s1, n1, s2, n2)                                              \
  MRBDBG(MRBDF(s1); MRBDH(n1); MRBDF(s2); MRBDH(n2))
#define MRBDSHSN(s1, n1, s2, n2)                                              \
  MRBDBG(MRBDF(s1); MRBDH(n1); MRBDF(s2); MRBDP(n2))
#define MRBDSNSH(s1, n1, s2, n2)                                              \
  MRBDBG(MRBDF(s1); MRBDP(n1); MRBDF(s2); MRBDH(n2))
#define MRBDSHSHS(s1, n1, s2, n2, s3)                                         \
  MRBDBG(MRBDF(s1); MRBDH(n1); MRBDF(s2); MRBDH(n2); MRBDF(s3))
#define MRBDSHSNS(s1, n1, s2, n2, s3)                                         \
  MRBDBG(MRBDF(s1); MRBDH(n1); MRBDF(s2); MRBDP(n2); MRBDF(s3))
#define MRBDSNSHS(s1, n1, s2, n2, s3)                                         \
  MRBDBG(MRBDF(s1); MRBDP(n1); MRBDF(s2); MRBDH(n2); MRBDF(s3))
#define MRBDSNSNSN(s1, n1, s2, n2, s3, n3)                                    \
  MRBDBG(MRBDF(s1); MRBDP(n1); MRBDF(s2); MRBDP(n2); MRBDF(s3); MRBDP(n3))
#define MRBDSHSNSN(s1, n1, s2, n2, s3, n3)                                    \
  MRBDBG(MRBDF(s1); MRBDH(n1); MRBDF(s2); MRBDP(n2); MRBDF(s3); MRBDP(n3))
#define MRBDSHSHSNS(s1, n1, s2, n2, s3, n3, s4)                               \
  MRBDBG(MRBDF(s1); MRBDH(n1); MRBDF(s2); MRBDH(n2); MRBDF(s3); MRBDP(n3);    \
         MRBDF(s4))

#define MRBSTOP(cmd)                                                          \
  {                                                                           \
    cmd;                                                                      \
    while (true)                                                              \
      ;                                                                       \
  };
