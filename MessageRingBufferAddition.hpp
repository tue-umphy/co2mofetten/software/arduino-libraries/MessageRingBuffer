#include <Arduino.h>

/*

PUSH - Append to HEAD

*/

template<size_t SIZE>
const MeRiBuAddStatus
MessageRingBuffer::push(const uint8_t (&buffer)[SIZE])
{
  return this->push(buffer, SIZE);
}

const MeRiBuAddStatus
MessageRingBuffer::push(const char buffer[], const size_t len)
{
  return this->push((const uint8_t*)buffer, len);
}

const MeRiBuAddStatus
MessageRingBuffer::push(const char buffer[])
{
  MRBDSNS("Request to push text '", buffer, "'");
  return this->push((const uint8_t*)buffer, strlen(buffer));
}

const MeRiBuAddStatus
MessageRingBuffer::push(const uint8_t buffer[], const size_t len)
{
  MRBDSN("Request to push buffer of size ", len);
  if (len == 0) {
    MRBDS("Refuse to push empty buffer");
    return MeRiBuAddStatus::EmptyInput;
  }
  // if the section buffer is smaller than the data it can not save it entirely
  if (len > this->maxPayloadSize()) {
    MRBDSN(
      "WARNING: ring buffer is too small to hold packet with payload size ",
      len);
#ifdef MessageRingBuffer_MRBDEBUG
    const size_t maxSize = this->maxPayloadSize();
    MRBDSNS("Maximum packet paylaod size is ", maxSize, " bytes");
#endif
    return MeRiBuAddStatus::SectionTooSmall;
  }
  MeRiBuAddStatus ret = MeRiBuAddStatus::Ok;
  if (not this->isEmpty()) {
    // drop packets from TAIL until there is enough room
    MRBDS("Making sure there is enough free space");
    while (!this->payloadFitsFree(len)) {
      this->dropTailPacket();
      ret = MeRiBuAddStatus::OkDataLoss;
    }
    // set HEAD to new packet address
    if (not this->isEmpty()) {
      this->setHeadAddr(this->nextPacketAddr(this->headAddr));
    }
  }
  // write packet size to new address
  MRBDSNS("Writing size of new packet (", len, ")");
  twobyte_t packetSize;
  packetSize.word = len;
  this->write(this->wrapBufAddr(this->headAddr, MeRiBuPackOffset(SizeHigh)),
              packetSize.byte[1]);
  this->write(this->wrapBufAddr(this->headAddr, MeRiBuPackOffset(SizeLow)),
              packetSize.byte[0]);
  // write packet data from buffer to MeRiBu
  MRBDS("Writing packet data");
  uint8_t crc;
  crc8_maxim_init(crc);
  for (size_t i = 0; i < len; i++) {
    crc8_maxim_update(crc, buffer[i]);
    this->write(this->packetPayloadAddr(this->headAddr, i), buffer[i]);
  }
  MRBDSHS("Writing checksum ", crc, " into packet metadata");
  this->write(this->wrapBufAddr(this->headAddr, MeRiBuPackOffset(Checksum)),
              crc);
  // Update PacketCount
  this->setPacketCount(this->packetCount + 1);

  return ret;
}

/*

UNSHIFT - Prepend to TAIL

*/

template<size_t SIZE>
const MeRiBuAddStatus
MessageRingBuffer::unshift(const uint8_t (&buffer)[SIZE])
{
  return this->unshift(buffer, SIZE);
}

const MeRiBuAddStatus
MessageRingBuffer::unshift(const char buffer[], const size_t len)
{
  return this->unshift((const uint8_t*)buffer, len);
}

const MeRiBuAddStatus
MessageRingBuffer::unshift(const char buffer[])
{
  MRBDSNS("Request to unshift text '", buffer, "'");
  return this->unshift((const uint8_t*)buffer, strlen(buffer));
}

const MeRiBuAddStatus
MessageRingBuffer::unshift(const uint8_t buffer[], const size_t len)
{
  MRBDSN("Request to unshift buffer of size ", len);
  if (len == 0) {
    MRBDS("Refuse to unshift empty buffer");
    return MeRiBuAddStatus::EmptyInput;
  }
  // if the section buffer is smaller than the data it can not save it entirely
  if (len > this->maxPayloadSize()) {
    MRBDSN(
      "WARNING: ring buffer is too small to hold packet with payload size ",
      len);
#ifdef MessageRingBuffer_MRBDEBUG
    const size_t maxSize = this->maxPayloadSize();
    MRBDSNS("Maximum packet paylaod size is ", maxSize, " bytes");
#endif
    return MeRiBuAddStatus::SectionTooSmall;
  }
  MeRiBuAddStatus ret = MeRiBuAddStatus::Ok;
  if (not this->isEmpty()) {
    // drop packets from HEAD until there is enough room
    MRBDS("Making sure there is enough free space");
    while (!this->payloadFitsFree(len)) {
      this->dropHeadPacket();
      ret = MeRiBuAddStatus::OkDataLoss;
    }
    // set TAIL to new packet address
    if (not this->isEmpty()) {
      const size_t offset = len + MeRiBuPackOffset(PacketMetaDataSize);
      MRBDSNSHS("Determining address ",
                offset,
                " bytes before TAIL (",
                this->tailAddr,
                ")");
      this->setTailAddr(this->wrapBufAddr(this->tailAddr, -(int32_t)offset));
    }
  }
  // write packet size to new address
  MRBDSNS("Writing size of new packet (", len, ")");
  twobyte_t packetSize;
  packetSize.word = len;
  this->write(this->wrapBufAddr(this->tailAddr, MeRiBuPackOffset(SizeHigh)),
              packetSize.byte[1]);
  this->write(this->wrapBufAddr(this->tailAddr, MeRiBuPackOffset(SizeLow)),
              packetSize.byte[0]);
  // write packet data from buffer to MeRiBu
  MRBDS("Writing packet data");
  uint8_t crc;
  crc8_maxim_init(crc);
  for (size_t i = 0; i < len; i++) {
    crc8_maxim_update(crc, buffer[i]);
    this->write(this->packetPayloadAddr(this->tailAddr, i), buffer[i]);
  }
  MRBDSHS("Writing checksum ", crc, " into packet metadata");
  this->write(this->wrapBufAddr(this->tailAddr, MeRiBuPackOffset(Checksum)),
              crc);
  // Update PacketCount
  this->setPacketCount(this->packetCount + 1);
  return ret;
}
