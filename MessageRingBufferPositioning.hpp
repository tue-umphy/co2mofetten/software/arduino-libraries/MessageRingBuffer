#include <Arduino.h>

const bufaddr_t
MessageRingBuffer::wrappedOffsetAddr(const bufaddr_t minAddr,
                                     const bufaddr_t maxAddr,
                                     const bufaddr_t addr,
                                     const int32_t offset)
{
  const size_t totalSize = maxAddr - minAddr + 1;
  /* MRBDSN("totalSize = ", totalSize); */
  const int32_t virtualNewAddr = (int32_t)addr + offset;
  /* MRBDSH("virtualNewAddr = ", virtualNewAddr); */
  const int32_t minOffset = (virtualNewAddr - minAddr) % totalSize;
  /* MRBDSN("minOffset = ", minOffset); */
  return minOffset >= 0 ? (minAddr + minOffset) : (maxAddr + minOffset + 1);
}

// calculate section address given a relative section offset

const bufaddr_t
MessageRingBuffer::secAddr(const uint16_t section_offset)
{
  return this->startAddrOfSection + section_offset;
}

// calculate buffer address given a relative buffer offset

const bufaddr_t
MessageRingBuffer::bufAddr(const size_t buffer_offset)
{
  return this->secAddr(MeRiBuSecOffset(SectionMetaDataSize)) + buffer_offset;
}

// calculate a buffer address given a relative buffer offset and wrap it

const bufaddr_t
MessageRingBuffer::wrapBufAddr(const bufaddr_t addr, const int32_t offset)
{
  return this->wrappedOffsetAddr(
    this->bufAddr(0), this->endAddrOfSection, addr, offset);
}

// calculate the address of a specific byte in a packet payload

const bufaddr_t
MessageRingBuffer::packetPayloadAddr(const bufaddr_t packetAddr,
                                     const size_t offset)
{
  return this->wrapBufAddr(packetAddr,
                           MeRiBuPackOffset(PacketMetaDataSize) + offset);
}

// determine whether the ring buffer is empty (i.e. contains no packets)

const bool
MessageRingBuffer::isEmpty(void)
{
  return this->headAddr == this->tailAddr and this->packetCount == 0;
}

// determine the free size for a new payload

const size_t
MessageRingBuffer::maxFreePayloadSize(void)
{
  return this->freeBufferSize() - MeRiBuPackOffset(PacketMetaDataSize);
}

// determine the maximum possible size for a payload

const size_t
MessageRingBuffer::maxPayloadSize(void)
{
  return this->totalBufferSize() - MeRiBuPackOffset(PacketMetaDataSize);
}

const size_t
MessageRingBuffer::totalBufferSize(void)
{
  return this->endAddrOfSection - this->bufAddr(0) + 1;
}

const size_t
MessageRingBuffer::usedBufferSize(void)
{
  if (this->tailAddr == this->headAddr) {
    return this->isEmpty() ? 0 : this->totalBufferSize();
  } else if (this->tailAddr < this->headAddr) {
    return this->headAddr - this->tailAddr;
  } else if (this->tailAddr > this->headAddr) {
    return this->totalBufferSize() - (this->tailAddr - this->headAddr);
  } else {
    return this->totalBufferSize();
  }
}

const size_t
MessageRingBuffer::freeBufferSize(void)
{
  size_t size;
  if (this->isEmpty()) {
    return this->totalBufferSize();
  }
  const size_t headPacketSize = this->getPacketPayloadSize(this->headAddr) +
                                MeRiBuPackOffset(PacketMetaDataSize);
  if (this->tailAddr == this->headAddr) {
    size = this->totalBufferSize() - headPacketSize;
  } else if (this->tailAddr < this->headAddr) {
    size = this->totalBufferSize() - (this->headAddr - this->tailAddr) -
           headPacketSize;
  } else if (this->tailAddr > this->headAddr) {
    size = this->tailAddr - this->headAddr - headPacketSize;
  } else {
    MRBDS("WARNING: You should never see this!");
    size = 0;
  }
  MRBDSNS("Free buffer size is ", size, " bytes");
  return size;
}

const size_t
MessageRingBuffer::getPacketPayloadSize(const bufaddr_t packetAddr)
{
  MRBDSH("Determining size of packet at address ", packetAddr);
  twobyte_t value;
  value.byte[1] = this->read(packetAddr);
  value.byte[0] = this->read(packetAddr + 1);
  if (value.word > this->maxPayloadSize()) {
    MRBDSHSNSN("WARNING: Packet at address ",
               packetAddr,
               " claims to have a payload size of ",
               value.word,
               " bytes which is greater than the maximum payload size ",
               this->maxPayloadSize());
  } else if (value.word > 0) {
    MRBDSHSN("Size of packet at address ", packetAddr, " is ", value.word);
  } else {
    MRBDSHS("WARNING: packet at address ", packetAddr, " has zero size");
  }
  return value.word;
}

const bufaddr_t
MessageRingBuffer::nextPacketAddr(const bufaddr_t packetAddr)
{
  bufaddr_t nextAddr =
    this->wrapBufAddr(packetAddr,
                      this->getPacketPayloadSize(packetAddr) +
                        MeRiBuPackOffset(PacketMetaDataSize));
  if (nextAddr == packetAddr)
    nextAddr += MeRiBuPackOffset(PacketMetaDataSize);
  MRBDSHSH("Next packet address after ", packetAddr, " is ", nextAddr);
  return nextAddr;
}

const bufaddr_t
MessageRingBuffer::previousPacketAddr(const bufaddr_t packetAddr)
{
  MRBDSH("Finding packet address preceding packet at ", packetAddr);
  if (this->isEmpty() or this->packetCount == 1) {
    MRBDSHSHS("packetCount=0 or 1, thus packet preciding address ",
              this->headAddr,
              " is TAIL (",
              this->tailAddr,
              ")");
    return this->tailAddr;
  }
  MRBDSH("Starting at TAIL address ", this->tailAddr);
  bufaddr_t addr = this->tailAddr; // start with TAIL
  bufaddr_t previousAddr = addr;
  while (true) {
    addr = this->nextPacketAddr(addr);
    if (addr == this->headAddr) {
      return previousAddr;
    }
    previousAddr = addr;
  }
}

// return the size of the HEAD packet

const size_t
MessageRingBuffer::headPayloadSize(void)
{
  MRBDS("Retrieving size of packet at HEAD address");
  if (this->isEmpty()) {
    MRBDSHS("Buffer is empty. Size of packet at HEAD address ",
            this->headAddr,
            " is 0");
    return 0;
  } else {
    size_t packetSize = this->getPacketPayloadSize(this->headAddr);
    MRBDSHSN(
      "Size of packet at HEAD address ", this->headAddr, " is ", packetSize);
    return packetSize;
  }
}

// return the size of the TAIL packet

const size_t
MessageRingBuffer::tailPayloadSize(void)
{
  MRBDS("Retrieving size of packet at TAIL address");
  if (this->isEmpty()) {
    MRBDSHS("Buffer is empty. Size of packet at TAIL address ",
            this->tailAddr,
            " is 0");
    return 0;
  } else {
    size_t packetSize = this->getPacketPayloadSize(this->tailAddr);
    MRBDSHSN(
      "Size of packet at TAIL address ", this->tailAddr, " is ", packetSize);
    return packetSize;
  }
}

// determine whether adding a packet of size len fits into the free buffer part

const bool
MessageRingBuffer::payloadFitsFree(const size_t len)
{
  size_t packetSize = (len + MeRiBuPackOffset(PacketMetaDataSize));
  if (this->freeBufferSize() >= packetSize) {
    MRBDSNSNS("A payload of ",
              len,
              " bytes plus ",
              MeRiBuPackOffset(PacketMetaDataSize),
              " bytes of packet metadata fits");
    return true;
  } else {
    MRBDSNSNS(
      "A payload of ",
      len,
      " bytes plus ",
      MeRiBuPackOffset(PacketMetaDataSize),
      " bytes of packet metadata does not fit without removing other packets");
    return false;
  }
}
