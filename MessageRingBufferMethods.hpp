#include <Arduino.h>

#include <MessageRingBufferAccessors.hpp>
#include <MessageRingBufferAddition.hpp>
#include <MessageRingBufferPositioning.hpp>
#include <MessageRingBufferRetrieval.hpp>

MessageRingBuffer::MessageRingBuffer(void) {}

const bool
MessageRingBuffer::begin(const bufaddr_t minStorageAddr,
                         const bufaddr_t maxStorageAddr,
                         const bufaddr_t startAddrOfSection,
                         const bufaddr_t endAddrOfSection,
                         readfunc_t readStorage,
                         writefunc_t writeStorage)
{
  MRBDSHSH("Beginning ring buffer in address region ",
           startAddrOfSection,
           " - ",
           endAddrOfSection);
  // the area needs a minimum size. It is the size of:
  //  SectionMetaDataSize + 1 (1 for an datapacket)
  if (endAddrOfSection <
      startAddrOfSection + MeRiBuSecOffset(SectionMetaDataSize) + 1) {
    MRBDS("The choosen section size is not large enough.");
    return false;
  }
  // save the variables into the private variables
  this->startAddrOfSection = startAddrOfSection;
  this->endAddrOfSection = endAddrOfSection;
  this->minStorageAddr = minStorageAddr;
  this->maxStorageAddr = maxStorageAddr;
  this->writeStorage = writeStorage;
  this->readStorage = readStorage;
  // reset access counters
  this->resetAccessCounters();
  // read metadata
  this->getHeadAddr();
  this->getTailAddr();
  this->getPacketCount();
  return true;
}

void
MessageRingBuffer::getVersion(uint8_t& major, uint8_t& minor, uint8_t& patch)
{
  MRBDS("Reading version of ring buffer previously written to MeRiBu");
  major = this->read(this->startAddrOfSection + MeRiBuSecOffset(VersionMajor));
  minor = this->read(this->startAddrOfSection + MeRiBuSecOffset(VersionMinor));
  patch = this->read(this->startAddrOfSection + MeRiBuSecOffset(VersionPatch));
  MRBDSNSNSN("ring buffer version previously written to MeRiBu: ",
             major,
             ".",
             minor,
             ".",
             patch);
}

void
MessageRingBuffer::writeLibraryVersion(void)
{
  MRBDS("Writing library version");
  this->writeVersion(MessageRingBuffer_version_major,
                     MessageRingBuffer_version_minor,
                     MessageRingBuffer_version_patch);
}

void
MessageRingBuffer::writeVersion(const uint8_t major,
                                const uint8_t minor,
                                const uint8_t patch)
{
  this->write(this->startAddrOfSection + MeRiBuSecOffset(VersionMajor), major);
  this->write(this->startAddrOfSection + MeRiBuSecOffset(VersionMinor), minor);
  this->write(this->startAddrOfSection + MeRiBuSecOffset(VersionPatch), patch);
}

const bool
MessageRingBuffer::versionCompatible(void)
{
  uint8_t major;
  uint8_t minor;
  uint8_t patch;
  this->getVersion(major, minor, patch);
  return semanticVersionCompatible(MessageRingBuffer_version_major,
                                   MessageRingBuffer_version_minor,
                                   MessageRingBuffer_version_patch,
                                   major,
                                   minor,
                                   patch);
}

// reset the ring buffer by
// - setting head and tail address to the address where the buffer begins
// - writing the library version into section metadata
// - setting the PacketCount to Empty

void
MessageRingBuffer::reset(void)
{
  bufaddr_t bufStart = this->bufAddr(0);
  this->writeLibraryVersion();
  this->setHeadAddr(bufStart);
  this->setTailAddr(bufStart);
  this->setPacketCount(0);
}

void
MessageRingBuffer::wipe(void)
{
  MRBDSHSHS("Wipe all data in the specified section (",
            this->startAddrOfSection,
            " to ",
            this->endAddrOfSection,
            ")");
  this->writeRegion(this->startAddrOfSection, this->endAddrOfSection);
  this->reset();
}

// gib Anzahl an abgelegten Paketen zurück, indem bei `start` begonnen wird,
// und immer die entsprechende Anzahl bytes weitergegangen wird, bis man
// (exakt) bei `last` landet

const size_t
MessageRingBuffer::count(void)
{
  /* bufaddr_t addr = this->tailAddr; */
  uint16_t nPackets = 0;
  size_t roundTrips = 0;
  while (headAddr != tailAddr and roundTrips < 2) {
    // TODO: This does not work obviously
    /* bufaddr_t nextAddr = this->nextPacketAddr(addr); */
    nPackets++;
  }
  return nPackets;
}

void
MessageRingBuffer::writeRegion(const bufaddr_t startAddr,
                               const bufaddr_t endAddr,
                               const uint8_t value)
{
  bufaddr_t addr = startAddr;
  while (true) {
    this->write(addr, value);
    if (addr++ == endAddr)
      break;
  }
}

void
MessageRingBuffer::writeBufferRegion(const bufaddr_t startAddr,
                                     const bufaddr_t endAddr,
                                     const uint8_t value)
{
  size_t i = 0;
  while (true) {
    const bufaddr_t addr = this->wrapBufAddr(startAddr, i++);
    if (addr == endAddr)
      break;
    this->write(addr, value);
  }
}

void
MessageRingBuffer::dump(Print& output,
                        const bufaddr_t startAddr,
                        const bufaddr_t endAddr,
                        const size_t columns,
                        const bool showChar,
                        const bool hideOther)
{
  MRBDSHSH("MeRiBu Message Ring Buffer content from address ",
           startAddr,
           " to ",
           endAddr);
  // determine effective start and end addresses to display in the table
  const bufaddr_t realStartAddr = startAddr - (startAddr % columns);
  const bufaddr_t realEndAddr = (endAddr - (endAddr % columns) + columns) - 1;
  MRBDSHSH(
    "Will effectively print addresses ", realStartAddr, " to ", realEndAddr);
  const size_t rows = (realEndAddr - (uint32_t)realStartAddr + 1) / columns;
  const size_t markerSize = 3;
  const char leftMarker[markerSize + 1] = "[({";
  const char rightMarker[markerSize + 1] = "]})";
  const bufaddr_t leftMarkerAddr[markerSize] = { this->startAddrOfSection,
                                                 this->headAddr,
                                                 this->tailAddr };
  const bufaddr_t rightMarkerAddr[markerSize] = { this->endAddrOfSection,
                                                  this->tailAddr,
                                                  this->headAddr };
  MRBDSNSNS("Will print ", rows, " rows with ", columns, " columns");
  output.println(F("Ring Buffer Dump: [] = section, () = HEAD, {} = TAIL"));
  for (size_t row = 0; row < rows; row++) {
    // print header
    if (row == 0) {
      // column header indent
      for (size_t i = 0; i < 7; i++)
        output.print(F(" "));
      // column headers
      for (size_t i = 0; i < columns; i++) {
        output.print(F(" "));
        printHexZeroPad(output, 2, i);
        output.print(F(" "));
      }
      output.println();
      // horizontal line
      for (size_t i = 0; i < 7 + (columns * 4); i++)
        output.print(F("-"));
      output.println();
    }
    output.print(F(" "));
    printHexZeroPad(output, 4, realStartAddr + (row * columns));
    output.print(F(" |"));
    for (size_t col = 0; col < columns; col++) {
      const bufaddr_t addr = realStartAddr + (row * columns) + col;
      const uint8_t value = this->read(addr, true);
      const bool inside =
        this->startAddrOfSection <= addr and addr <= this->endAddrOfSection;
      bool leftMarkerPrinted = false;
      bool rightMarkerPrinted = false;
      // print left marker
      for (size_t i = 0; i < markerSize; i++) {
        if (addr == leftMarkerAddr[i]) {
          output.print(leftMarker[i]);
          leftMarkerPrinted = true;
          break;
        }
      }
      if (not leftMarkerPrinted)
        output.print(F(" "));
      // print value
      if (not inside and hideOther) {
        output.print(F(".."));
      } else {
        printHexZeroPad(output, 2, value);
      }
      // print right marker
      for (size_t i = 0; i < markerSize; i++) {
        if (addr == rightMarkerAddr[i]) {
          output.print(rightMarker[i]);
          rightMarkerPrinted = true;
          break;
        }
      }
      if (not rightMarkerPrinted)
        output.print(F(" "));
    }
    // if desired, show the ASCII represenation
    if (showChar) {
      output.print(" | ");
      for (size_t col = 0; col < columns; col++) {
        const bufaddr_t addr = realStartAddr + (row * columns) + col;
        const uint8_t value = this->read(addr, true);
        const bool inside =
          this->startAddrOfSection <= addr and addr <= this->endAddrOfSection;
        if (not inside and hideOther) {
          output.print(F(" "));
        } else {
          // turn problematic characters into spaces
          output.print(not(0x20 <= value and value <= 0x7F) ? (char)0x20
                                                            : (char)value);
        }
      }
      output.print(" |");
    }
    output.println();
  }
}

void
MessageRingBuffer::dump(Print& output,
                        const size_t columns,
                        const bool showChar,
                        const bool hideOther)
{
  this->dump(output,
             this->startAddrOfSection,
             this->endAddrOfSection,
             columns,
             showChar,
             hideOther);
}

void
MessageRingBuffer::write(const bufaddr_t addr,
                         const uint8_t value,
                         const bool silent)
{
  bool doWrite = false;
  if (this->onlyWriteIfNecessary)
    doWrite = this->read(addr) != value;
  if (doWrite) {
    if (not silent)
      MRBDSHSHSNS("write [", addr, "] = ", value, " (", (char)value, ")");
    this->writeStorage(addr, value);
    this->writeCount++;
  }
}

const uint8_t
MessageRingBuffer::read(const bufaddr_t addr, const bool silent)
{
  const uint8_t value = this->readStorage(addr);
  if (not silent)
    MRBDSHSHSNS(" read [", addr, "] = ", value, " (", (char)value, ")");
  this->readCount++;
  return value;
}
