#include <Arduino.h>

template<typename T>
void
printHexZeroPad(Print& output, const size_t width, const T value)
{
  size_t printWidth = 0;
  T val = value;
  while (val) {
    printWidth++;
    val >>= 4;
  }
  for (size_t i = printWidth; i < width; i++) {
    output.print(F("0"));
  }
  if (value)
    output.print(value, HEX);
}
