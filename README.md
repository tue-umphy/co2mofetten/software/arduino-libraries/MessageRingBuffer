# MessageRingBuffer Arduino Library

Arduino library for a ring buffer on an arbitrary byte storage 
(e.g. SRAM, FRAM, EEPROM, ...) holding arbitrary-sized messages.

In this ring buffer you can push, pop and unshift, shift data with an
independent data size length.

## Documentation

For now, see the
[`MessageRingBuffer.hpp`](https://gitlab.com/tue-umphy/co2mofetten/arduino-libraries/MessageRingBuffer/blob/master/MessageRingBuffer.hpp)
file for a method description or have a look at the `examples` folder.



