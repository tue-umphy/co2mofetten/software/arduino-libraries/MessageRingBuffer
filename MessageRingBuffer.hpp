#ifndef MessageRingBuffer_hpp
#define MessageRingBuffer_hpp

/*

# MessageRingBuffer library

## Terms

- Section
  - address region inside the buffer where the ring buffer is placed
- Buffer
  - address region after the section metadata block
- Packet
  - combination of payload and metadata (e.g. size)
  - Payload
    - the actual data that is stored
- Head (slightly counter-intuitive at first, imagine a snake!)
  - position of (the metadata block of) the last push()ed element
- Tail (slightly counter-intuitive at first, imagine a snake!)
  - position of (the metadata block of) the last unshift()ed element

## Structure

- Section:
  - Metadata
    - 3 bytes for version number
    - 2 bytes headaddress
    - 2 bytes tailaddress
    - 2 bytes packet count
  - Data Packets, each
    - Metadata
      - 2 bytes payload size
      - 1 byte payload CRC checksum

## Notes

- Multi-byte integers are always read and written with the high byte first

*/

#include <Arduino.h>
#include <MessageRingBufferDebug.h>
#include <MessageRingBufferUtils.hpp>
#include <MessageRingBufferVersion.h>

// combines bytes to a word and vice-versa
// WARNING: the order of the bytes is compiler-dependant!
union twobyte_t
{
  uint16_t word;
  uint8_t byte[sizeof(uint16_t)];
};

enum class MeRiBuSectionMetaDataOffset : uint8_t
{
  VersionMajor,
  VersionMinor,
  VersionPatch,
  HeadAddrHigh,
  HeadAddrLow,
  TailAddrHigh,
  TailAddrLow,
  PacketCountHigh,
  PacketCountLow,
  SectionMetaDataSize, // size of metadata block in bytes
};

#define MeRiBuSecOffset(m) ((uint8_t)MeRiBuSectionMetaDataOffset::m)

enum class MeRiBuPacketMetaDataOffset : uint8_t
{
  SizeHigh,
  SizeLow,
  Checksum,
  PacketMetaDataSize, // size of metadata block in bytes
};

#define MeRiBuPackOffset(m) ((uint8_t)MeRiBuPacketMetaDataOffset::m)

enum class MeRiBuAddStatus
{
  Ok,
  OkDataLoss,
  EmptyInput,
  SectionTooSmall,
  AddStatusSize, // number of AddStatus values
};

enum class MeRiBuRetrieveStatus
{
  Ok,
  RingBufferIsEmpty,
  OutputBufferTooSmall,
  InvalidChecksum,
  RetrieveStatusSize, // number of RetrieveStatus values
};

typedef uint16_t bufaddr_t;

class MessageRingBuffer
{
  // typedefs for storage read and write function pointers
  typedef void (*writefunc_t)(const bufaddr_t addr, const uint8_t value);
  typedef const uint8_t (*readfunc_t)(const bufaddr_t addr);

public:
  // constructor
  MessageRingBuffer(void);

  // begin a ring buffer on a specified backend
  const bool begin(const bufaddr_t minStorageAddr,
                   const bufaddr_t maxStorageAddr,
                   const bufaddr_t startAddrOfSection,
                   const bufaddr_t endAddrOfSection,
                   MessageRingBuffer::readfunc_t readStorage,
                   MessageRingBuffer::writefunc_t writeStorage);

  // calculate section address given a relative section offset
  const bufaddr_t secAddr(const size_t section_offset);
  // calculate buffer address given a relative buffer offset
  const bufaddr_t bufAddr(const size_t buffer_offset);
  // wrap an address position in a buffer
  const bufaddr_t wrappedOffsetAddr(const bufaddr_t minAddr,
                                    const bufaddr_t maxAddr,
                                    const bufaddr_t addr,
                                    const int32_t offset);
  // calcualte wrapped address position in buffer
  const bufaddr_t wrapBufAddr(const bufaddr_t addr, const int32_t offset);
  // calculate a paylaod address given its packet address and an offset
  const bufaddr_t packetPayloadAddr(const bufaddr_t packetAddr,
                                    const size_t offset);

  // read the MessageRingBuffer version previously written to MeRiBu
  void getVersion(uint8_t& major, uint8_t& minor, uint8_t& patch);

  // check whether MessageRingBuffer version previously written to MeRiBu is
  // compatible with this library version
  const bool versionCompatible(void);

  // write this MessageRingBuffer library version to MeRiBu
  void writeLibraryVersion(void);

  // reset the ring buffer by
  // - setting head and tail address to the address where the buffer begins
  // - writing the library version into section metadata
  // - setting the PacketCount to Empty
  void reset(void);

  // wipe the ring buffer with zeroes and reset()
  void wipe(void);

  // shift()
  // retrieve and drop the foremost packet
  const MeRiBuRetrieveStatus shift(void);
  const MeRiBuRetrieveStatus shift(uint8_t buffer[],
                                   const size_t len,
                                   const bool drop = true);
  template<size_t SIZE>
  const MeRiBuRetrieveStatus shift(uint8_t (&buffer)[SIZE],
                                   const bool drop = true);
  template<size_t SIZE>
  const MeRiBuRetrieveStatus shift(char (&buffer)[SIZE],
                                   const bool drop = true);
  // pop()
  // retrieve and drop the foremost packet
  const MeRiBuRetrieveStatus pop(void);
  const MeRiBuRetrieveStatus pop(uint8_t buffer[],
                                 const size_t len,
                                 const bool drop = true);
  template<size_t SIZE>
  const MeRiBuRetrieveStatus pop(uint8_t (&buffer)[SIZE],
                                 const bool drop = true);
  template<size_t SIZE>
  const MeRiBuRetrieveStatus pop(char (&buffer)[SIZE], const bool drop = true);

  // push()
  // - append a packet to the rightmost side (HEAD) of the ring buffer
  const MeRiBuAddStatus push(const char buffer[], const size_t len);
  const MeRiBuAddStatus push(const char buffer[]);
  const MeRiBuAddStatus push(const uint8_t buffer[], const size_t len);
  template<size_t SIZE>
  const MeRiBuAddStatus push(const uint8_t (&buffer)[SIZE]);

  // unshift()
  // - prepend a packet to the leftmost side (TAIL) of the ring buffer
  const MeRiBuAddStatus unshift(const char buffer[], const size_t len);
  const MeRiBuAddStatus unshift(const char buffer[]);
  const MeRiBuAddStatus unshift(const uint8_t buffer[], const size_t len);
  template<size_t SIZE>
  const MeRiBuAddStatus unshift(const uint8_t (&buffer)[SIZE]);

  const size_t headPayloadSize(void);
  const size_t tailPayloadSize(void);
  const bufaddr_t nextPacketAddr(const bufaddr_t packetAddr);
  const bufaddr_t previousPacketAddr(const bufaddr_t packetAddr);

  // lies LETZTES Element in `buffer` ein und vergiss es, überprüfe
  // CRC8-Prüfsumme und gib Status zurück // return bitmap
  const MeRiBuRetrieveStatus pop(char buffer[]);

  // gib Anzahl an abgelegten Elementen zurück, indem bei `start` begonnen
  // wird, und immer die entsprechende Anzahl bytes weitergegangen wird, bis
  // man (exakt) bei `last` landet
  const size_t count(void);

  // gib zurück, ob Ringpuffer leer ist (d.h. `first` == `last`)
  const bool isEmpty(void);

  // determine the maximum possible size for a packet new fitting packet
  const size_t maxFreePayloadSize(void);
  // determine the maximum possible size for a packet
  const size_t maxPayloadSize(void);

  // total size of the buffer
  const size_t totalBufferSize(void);
  // free size of the buffer
  const size_t freeBufferSize(void);
  // used size of the buffer
  const size_t usedBufferSize(void);
  // determine whether a packet fits into the free part of the buffer
  const bool payloadFitsFree(const size_t len);

  // start and end addresses of the section
  const bufaddr_t getStartAddrOfSection(void);
  const bufaddr_t getEndAddrOfSection(void);

  // dump a MeRiBu address region in a pretty table
  void dump(Print& output,
            const bufaddr_t startAddr,
            const bufaddr_t endAddr,
            const size_t columns = 16,
            const bool showChar = true,
            const bool hideOther = true);
  // dump the ringBuffer address region
  void dump(Print& output,
            const size_t columns = 16,
            const bool showChar = true,
            const bool hideOther = true);

  // whether to clean up dropped packet regions with cleanUpValue
  // (not recommended, for debugging purposes only)
  bool cleanUpOnDrop = false;
  // whether to check if writing to an address is necessary (i.e. it contains a
  // different value). This is the default and helps reduce wearing of FRAM
  // and EEPROM for example.
  bool onlyWriteIfNecessary = true;
  uint8_t cleanUpValue = 0x00;

  const unsigned long getReadCount(void);
  const unsigned long getWriteCount(void);
  void resetAccessCounters(void);

private:
  bufaddr_t headAddr;
  bufaddr_t tailAddr;
  unsigned long readCount;
  unsigned long writeCount;
  size_t packetCount;
  const bufaddr_t getHeadAddr(void);
  const bufaddr_t getTailAddr(void);
  void setHeadAddr(bufaddr_t addr);
  void setTailAddr(bufaddr_t addr);
  const size_t getPacketCount(void);
  void setPacketCount(const size_t count);

  const uint16_t getPacketPayloadSize(const bufaddr_t packetAddr);
  const bool dropTailPacket(void);
  const bool dropHeadPacket(void);

  void write(const bufaddr_t addr,
             const uint8_t value,
             const bool silent = false);
  const uint8_t read(const bufaddr_t addr, const bool silent = false);

  void writeVersion(const uint8_t major,
                    const uint8_t minor,
                    const uint8_t patch);
  void writeRegion(const bufaddr_t startAddr,
                   const bufaddr_t endAddr,
                   const uint8_t value = 0x00);
  void writeBufferRegion(const bufaddr_t startAddr,
                         const bufaddr_t endAddr,
                         const uint8_t value = 0x00);

  // storage management
  readfunc_t readStorage;
  writefunc_t writeStorage;
  bufaddr_t minStorageAddr;
  bufaddr_t maxStorageAddr;
  bufaddr_t endAddrOfSection;
  bufaddr_t startAddrOfSection;
};

// check if version 1 is compatible to version 2
// according to Semantic Versioning (see https://semver.org/)
const bool
semanticVersionCompatible(const uint8_t major1,
                          const uint8_t minor1,
                          const uint8_t patch1,
                          const uint8_t major2,
                          const uint8_t minor2,
                          const uint8_t patch2)
{
  // major version MUST match, because major versions introduce
  // backwards-INcompatible changes
  return major1 == major2 &&
         // If still in development (i.e. minor==0), minor version MUST match,
         // otherwise be greater or equal
         (minor1 ? (minor1 >= minor2) : (minor1 == minor2)) &&
         // patch version must be greater or equal
         patch1 >= patch2;
}

// CRC8 Checksum
void
crc8_maxim_init(uint8_t& crc);
void
crc8_maxim_update(uint8_t& crc, uint8_t d);

#include <MessageRingBufferMethods.hpp>

#endif // #ifndef MessageRingBuffer_hpp
