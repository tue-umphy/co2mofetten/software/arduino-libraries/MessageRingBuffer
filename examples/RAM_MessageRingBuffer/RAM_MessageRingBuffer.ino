#include <MessageRingBuffer.hpp>

// Library version in HEX: 0xMMmmpp (MM=major, mm=minor, pp=patch)
#if MessageRingBuffer_VERSION < 0x010100
#error "This sketch requires at least MessageRingBuffer version 1.1"
#endif

// currently, only 16-bit addresses (the default template argument) are
// supported.
MessageRingBuffer ringBuffer;

/*

  Create a buffer with access methods

*/
uint8_t buffer[0x111 + 1] = { 0 };

void
buffer_write(const uint16_t addr, const uint8_t value)
{
  buffer[addr] = value;
}

const uint8_t
buffer_read(const uint16_t addr)
{
  return buffer[addr];
}

void
setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println("############################");
  Serial.println("#          Example         #");
  Serial.println("# MessageRingBuffer on RAM #");
  Serial.println("############################");

  const bool ringbuffer_works = ringBuffer.begin(0x00,
                                                 0x111, // max region
                                                 // for demonstration we only
                                                 // use part of the buffer
                                                 0x96,
                                                 0x103,
                                                 // accessor functions
                                                 buffer_read,
                                                 buffer_write);
  if (not ringbuffer_works) {
    Serial.println("MessageRingBuffer could not start.");
    while (true)
      ;
  }
  ringBuffer.cleanUpOnDrop = true;

  ringBuffer.reset();
  ringBuffer.dump(Serial);

  // write multiple sentences into the MeRiBu.
  // print the status code
  char text[] = "How are you today?";
  MeRiBuAddStatus pushcode1 = ringBuffer.push(text);
  Serial.print("push code: \t");
  Serial.println((uint8_t)pushcode1);
  ringBuffer.dump(Serial);

  char text2[] = "I am fine, thank you.";
  MeRiBuAddStatus pushcode2 = ringBuffer.push(text2);
  Serial.print("push code: \t");
  Serial.println((uint8_t)pushcode2);
  ringBuffer.dump(Serial);

  char text3[] = "What a nice conversation!";
  MeRiBuAddStatus pushcode3 = ringBuffer.push(text3);
  Serial.print("push code: \t");
  Serial.println((uint8_t)pushcode3);
  ringBuffer.dump(Serial);

  uint8_t shifttext1[100] = { 0 };
  MeRiBuRetrieveStatus shiftcode1 = ringBuffer.shift(shifttext1);
  Serial.print("shift code: ");
  Serial.println((uint8_t)shiftcode1);
  Serial.print("shift()ed text: '");
  Serial.print((char*)shifttext1);
  Serial.println("'");
  ringBuffer.dump(Serial);

  char text4[] = "Very interesting talk...";
  MeRiBuAddStatus unshiftcode1 = ringBuffer.unshift(text4);
  Serial.print("unshift code: \t");
  Serial.println((uint8_t)unshiftcode1);
  ringBuffer.dump(Serial);

  uint8_t poptext2[100] = { 0 };
  Serial.println("pop() without dropping");
  MeRiBuRetrieveStatus popcode1 = ringBuffer.pop(poptext2, false);
  Serial.print("pop code: ");
  Serial.println((uint8_t)popcode1);
  Serial.print("pop()ed text: '");
  Serial.print((char*)poptext2);
  Serial.println("'");
  ringBuffer.dump(Serial);
  Serial.println("now pop() with drop");
  if (popcode1 == MeRiBuRetrieveStatus::Ok)
    ringBuffer.pop();
  else
    Serial.println("WARINING: pop() didn't work previously!?");
  ringBuffer.dump(Serial);

  uint8_t poptext1[100] = { 0 };
  MeRiBuRetrieveStatus popcode2 = ringBuffer.pop(poptext1);
  Serial.print("pop code: ");
  Serial.println((uint8_t)popcode2);
  Serial.print("pop()ed text: '");
  Serial.print((char*)poptext1);
  Serial.println("'");
  ringBuffer.dump(Serial);

  Serial.print(" Read Accesses: ");
  Serial.println(ringBuffer.getReadCount());
  Serial.print("Write Accesses: ");
  Serial.println(ringBuffer.getWriteCount());
}

void
loop()
{}
