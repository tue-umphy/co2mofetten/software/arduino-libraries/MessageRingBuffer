/*

  Example sketch for a MessageRingBuffer on the Arduino's EEPROM using
  ArduinoJson to write, retrieve, update and reinsert MsgPack data into the
  the ring buffer.

  WARNING: The EEPROM can only be written a limited amount of times. Don't run
  this sketch too often!

*/

#include <ArduinoJson.h>
#include <EEPROM.h>
#include <MessageRingBuffer.hpp>

// Library version in HEX: 0xMMmmpp (MM=major, mm=minor, pp=patch)
#if MessageRingBuffer_VERSION < 0x010000
#error "This sketch requires at least MessageRingBuffer version 1.0"
#endif

// currently, only 16-bit addresses (the default template argument) are
// supported.
MessageRingBuffer ringBuffer;

/*

  EEPROM access wrapper methods

*/
void
eeprom_write(const uint16_t addr, const uint8_t value)
{
  EEPROM[addr] = value;
}

const uint8_t
eeprom_read(const uint16_t addr)
{
  return EEPROM[addr];
}

void
createMsgPack(char payload[])
{
  StaticJsonDocument<750> doc;
  doc["station"] = random(0, 255);
  JsonArray ppm = doc.createNestedArray("scd_co2_ppm");
  for (size_t i = 0; i < random(1, 10); i++) {
    ppm.add(random(800, 1200));
  }
  JsonArray pressure = doc.createNestedArray("bme_pres_hpa");
  for (size_t i = 0; i < random(1, 10); i++) {
    pressure.add(random(900, 1100));
  }
  JsonArray tmp = doc.createNestedArray("scd_temp_c");
  for (size_t i = 0; i < random(1, 10); i++) {
    tmp.add(random(10, 30));
  }
  JsonArray lf = doc.createNestedArray("bme_rh_p");
  for (size_t i = 0; i < random(1, 10); i++) {
    lf.add(random(20, 80));
  }
  JsonArray utc = doc.createNestedArray("time_utc");
  for (size_t i = 0; i < random(1, 10); i++) {
    utc.add(random(1550491851, 1550578244));
  }
  size_t msgpack_size = measureMsgPack(doc);
  Serial.println("JSON payload:");
  serializeJson(doc, Serial);
  Serial.println();
  Serial.print("MsgPack length in bytes: ");
  Serial.println(msgpack_size);
  serializeMsgPack(doc, payload, msgpack_size + 1); // include null-byte
  Serial.print("MsgPack payload (");
  Serial.print(msgpack_size);
  Serial.println(" bytes) [HEX]:");
  for (size_t i = 0; i < msgpack_size; i++) {
    Serial.print((uint8_t)payload[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
}

void
setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println("#############################");
  Serial.println("#          Example          #");
  Serial.println("# MessageRingBuffer on FRAM #");
  Serial.println("#############################");

  const bool ringbuffer_works =
    ringBuffer.begin(0x00,
                     EEPROM.length() - 1, // max region
                     // for demonstration we only
                     // use part of the EEPROM
                     0x00,
                     0xA0,
                     // accessor functions
                     eeprom_read,
                     eeprom_write);
  if (not ringbuffer_works) {
    Serial.println("MessageRingBuffer could not start.");
    while (true)
      ;
  }
  ringBuffer.cleanUpOnDrop = false;
  uint8_t major;
  uint8_t minor;
  uint8_t patch;
  ringBuffer.getVersion(major, minor, patch);
  Serial.print("MessageRingBuffer version of buffer on EEPROM: ");
  Serial.print(major);
  Serial.print(".");
  Serial.print(minor);
  Serial.print(".");
  Serial.print(patch);
  Serial.println();
  Serial.print("MessageRingBuffer library version: ");
  Serial.print(MessageRingBuffer_version_major);
  Serial.print(".");
  Serial.print(MessageRingBuffer_version_minor);
  Serial.print(".");
  Serial.print(MessageRingBuffer_version_patch);
  Serial.println();
  Serial.print("MessageRingBuffer_VERSION macro in HEX: 0x");
  Serial.println(MessageRingBuffer_VERSION, HEX);
  if (ringBuffer.versionCompatible()) {
    Serial.print("MessageRingBuffer library version is compatible ");
    Serial.print("with version of library previously used to write EEPROM!");
    Serial.println();
  } else {
    Serial.println(
      "WARNING: MessageRingBuffer library version is incompatible!");
    Serial.println("Wiping EEPROM in ringbuffer region");
    ringBuffer.reset();
  }

  ringBuffer.dump(Serial);

  char msgpack_write[300];
  createMsgPack(msgpack_write);

  const MeRiBuAddStatus pushcode = ringBuffer.push(msgpack_write);
  Serial.print("push code: ");
  Serial.println((uint8_t)pushcode);
  ringBuffer.dump(Serial);

  uint8_t msgpack_read[300];
  const size_t headSize = ringBuffer.headPayloadSize();
  const MeRiBuRetrieveStatus popcode = ringBuffer.pop(msgpack_read);
  Serial.print("pop()ed MsgPack payload (");
  Serial.print(headSize);
  Serial.println(" bytes) [HEX]:");
  for (size_t i = 0; i < headSize; i++) {
    Serial.print((uint8_t)msgpack_read[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
  Serial.print("pop code: ");
  Serial.println((uint8_t)popcode);
  ringBuffer.dump(Serial);

  StaticJsonDocument<750> doc;
  DeserializationError err = deserializeMsgPack(doc, msgpack_read, headSize);
  if (err) {
    Serial.print(F("deserializeMsgPack() failed with "));
    Serial.println(err.c_str());
  } else {
    Serial.println(F("Deserialized pop()ed MsgPack as JSON: "));
    serializeJson(doc, Serial);
    Serial.println();
  }

  doc["extra"] = "data";
  Serial.println(F("Deserialized pop()ed MsgPack with extra data as JSON: "));
  serializeJson(doc, Serial);
  Serial.println();

  serializeMsgPack(doc, msgpack_write);
  const MeRiBuAddStatus pushcode2 = ringBuffer.push(msgpack_write);
  Serial.print("push code: ");
  Serial.println((uint8_t)pushcode2);
  ringBuffer.dump(Serial);

  Serial.print(" Read Accesses: ");
  Serial.println(ringBuffer.getReadCount());
  Serial.print("Write Accesses: ");
  Serial.println(ringBuffer.getWriteCount());
}

void
loop()
{}
