/*

Hardware Setup:
  - Connect the Adafruit FRAM breakout board via I²C and power it via 5V

*/

#include <Adafruit_FRAM_I2C.h>
#include <MessageRingBuffer.hpp>

// Library version in HEX: 0xMMmmpp (MM=major, mm=minor, pp=patch)
#if MessageRingBuffer_VERSION < 0x010000
#error "This sketch requires at least MessageRingBuffer version 1.0"
#endif

// currently, only 16-bit addresses (the default template argument) are
// supported.
MessageRingBuffer ringBuffer;
Adafruit_FRAM_I2C fram;

/*

  Wrapper functions for reading and writing from and to FRAM.

  Due to C++ (or my lack of understanding), it is not possible to use
  pointers to the member functions directly as that would also require to store
  a pointer to the instance (see https://stackoverflow.com/a/670744 or
  https://stackoverflow.com/a/4296290) which is not really more elegant.

*/
void
fram_write(const uint16_t addr, const uint8_t value)
{
  fram.write8(addr, value);
}

const uint8_t
fram_read(const uint16_t addr)
{
  return fram.read8(addr);
}

void
setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println("#############################");
  Serial.println("#          Example          #");
  Serial.println("# MessageRingBuffer on FRAM #");
  Serial.println("#############################");

  const bool fram_works = fram.begin();
  if (not fram_works) {
    Serial.println("FRAM could not start");
    while (true)
      ;
  }
  const bool ringbuffer_works = ringBuffer.begin(0x00,
                                                 0xFFFF, // max region
                                                 0x2D,
                                                 0x79,      // our region
                                                 fram_read, // read function
                                                 fram_write // write function
  );
  if (not ringbuffer_works) {
    Serial.println("MessageRingBuffer could not start.");
    while (true)
      ;
  }
  ringBuffer.cleanUpOnDrop = true;
  uint8_t major;
  uint8_t minor;
  uint8_t patch;
  ringBuffer.getVersion(major, minor, patch);
  Serial.print("MessageRingBuffer version of buffer on FRAM: ");
  Serial.print(major);
  Serial.print(".");
  Serial.print(minor);
  Serial.print(".");
  Serial.print(patch);
  Serial.println();
  Serial.print("MessageRingBuffer library version: ");
  Serial.print(MessageRingBuffer_version_major);
  Serial.print(".");
  Serial.print(MessageRingBuffer_version_minor);
  Serial.print(".");
  Serial.print(MessageRingBuffer_version_patch);
  Serial.println();
  Serial.print("MessageRingBuffer_VERSION macro in HEX: 0x");
  Serial.println(MessageRingBuffer_VERSION, HEX);
  if (ringBuffer.versionCompatible()) {
    Serial.print("MessageRingBuffer library version is compatible ");
    Serial.print("with version of library previously used to write FRAM!");
    Serial.println();
  } else {
    Serial.println(
      "WARNING: MessageRingBuffer library version is incompatible!");
  }

  Serial.println(F("Wiping Ring buffer on FRAM"));
  ringBuffer.wipe();
  ringBuffer.dump(Serial);

  // write multiple sentences into the MeRiBu.
  // print the status code
  char text[] = "How are you today?";
  MeRiBuAddStatus pushcode1 = ringBuffer.push(text);
  Serial.print("push code: \t");
  Serial.println((uint8_t)pushcode1);
  ringBuffer.dump(Serial);

  char text2[] = "I am fine, thank you.";
  MeRiBuAddStatus pushcode2 = ringBuffer.push(text2);
  Serial.print("push code: \t");
  Serial.println((uint8_t)pushcode2);
  ringBuffer.dump(Serial);

  char text3[] = "What a nice conversation!";
  MeRiBuAddStatus pushcode3 = ringBuffer.push(text3);
  Serial.print("push code: \t");
  Serial.println((uint8_t)pushcode3);
  ringBuffer.dump(Serial);

  uint8_t shifttext1[100] = { 0 };
  MeRiBuRetrieveStatus shiftcode1 = ringBuffer.shift(shifttext1);
  Serial.print("shift code: ");
  Serial.println((uint8_t)shiftcode1);
  Serial.print("shift()ed text: '");
  Serial.print((char*)shifttext1);
  Serial.println("'");
  ringBuffer.dump(Serial);

  char text4[] = "Very interesting talk...";
  MeRiBuAddStatus unshiftcode1 = ringBuffer.unshift(text4);
  Serial.print("unshift code: \t");
  Serial.println((uint8_t)unshiftcode1);
  ringBuffer.dump(Serial);

  uint8_t poptext2[100] = { 0 };
  MeRiBuRetrieveStatus popcode1 = ringBuffer.pop(poptext2);
  Serial.print("pop code: ");
  Serial.println((uint8_t)popcode1);
  Serial.print("pop()ed text: '");
  Serial.print((char*)poptext2);
  Serial.println("'");
  ringBuffer.dump(Serial);

  uint8_t poptext1[100] = { 0 };
  MeRiBuRetrieveStatus popcode2 = ringBuffer.pop(poptext1);
  Serial.print("pop code: ");
  Serial.println((uint8_t)popcode2);
  Serial.print("pop()ed text: '");
  Serial.print((char*)poptext1);
  Serial.println("'");
  ringBuffer.dump(Serial);

  Serial.print(" Read Accesses: ");
  Serial.println(ringBuffer.getReadCount());
  Serial.print("Write Accesses: ");
  Serial.println(ringBuffer.getWriteCount());
}

void
loop()
{}
