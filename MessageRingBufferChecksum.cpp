#include <Arduino.h>

void
crc8_maxim_init(uint8_t& crc)
{
  crc = 0x00;
}

void
crc8_maxim_update(uint8_t& crc, uint8_t d)
{
  for (uint8_t i = 8; i; i--) {
    const uint8_t sum = (crc ^ d) & 0x01;
    crc >>= 1;
    if (sum) {
      crc ^= 0x8C;
    }
    d >>= 1;
  }
}
