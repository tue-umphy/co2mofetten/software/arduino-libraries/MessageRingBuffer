#include <Arduino.h>

/*

  SHIFT - Retrieve and drop from TAIL

*/

template<size_t SIZE>
const MeRiBuRetrieveStatus
MessageRingBuffer::shift(uint8_t (&buffer)[SIZE], const bool drop)
{
  return this->shift(buffer, SIZE, drop);
}

template<size_t SIZE>
const MeRiBuRetrieveStatus
MessageRingBuffer::shift(char (&buffer)[SIZE], const bool drop)
{
  const MeRiBuRetrieveStatus ret = this->shift(buffer, SIZE - 2, drop);
  buffer[SIZE - 1] = 0x00;
  return ret;
}

const MeRiBuRetrieveStatus
MessageRingBuffer::shift(void)
{
  if (this->isEmpty()) {
    return MeRiBuRetrieveStatus::RingBufferIsEmpty;
  } else {
    this->dropTailPacket();
    return MeRiBuRetrieveStatus::Ok;
  }
}

const MeRiBuRetrieveStatus
MessageRingBuffer::shift(uint8_t buffer[], const size_t len, const bool drop)
{
  MRBDSN("Request to shift into a buffer of size ", len);
  if (this->isEmpty()) {
    MRBDS("RingBuffer is empty. Cannot shift()");
    return MeRiBuRetrieveStatus::RingBufferIsEmpty;
  }
  const size_t tailSize = this->tailPayloadSize();
  uint8_t crc;
  crc8_maxim_init(crc);
  for (size_t i = 0; i < tailSize; i++) {
    if (i >= len) {
      MRBDSNSN("WARNING: Cannot put all ",
               tailSize,
               " packet bytes into too small output buffer of size ",
               len);
      return MeRiBuRetrieveStatus::OutputBufferTooSmall;
    }
    buffer[i] = this->read(this->packetPayloadAddr(this->tailAddr, i));
    crc8_maxim_update(crc, buffer[i]);
  }
  MRBDS("Read written checksum");
  uint8_t crcOnBuf =
    this->read(this->wrapBufAddr(this->tailAddr, MeRiBuPackOffset(Checksum)));
  if (crcOnBuf == crc) {
    if (drop)
      this->dropTailPacket();
    else
      MRBDS("Keeping TAIL packet");
    return MeRiBuRetrieveStatus::Ok;
  } else {
    MRBDSHSH("WARNING: Calculated checksum ",
             crc,
             " does not match written packet checksum ",
             crcOnBuf);
    return MeRiBuRetrieveStatus::InvalidChecksum;
  }
}

/*

  POP - Retrieve and drop from HEAD

*/

const MeRiBuRetrieveStatus
MessageRingBuffer::pop(void)
{
  if (this->isEmpty()) {
    return MeRiBuRetrieveStatus::RingBufferIsEmpty;
  } else {
    this->dropHeadPacket();
    return MeRiBuRetrieveStatus::Ok;
  }
}

template<size_t SIZE>
const MeRiBuRetrieveStatus
MessageRingBuffer::pop(uint8_t (&buffer)[SIZE], const bool drop)
{
  return this->pop(buffer, SIZE, drop);
}

template<size_t SIZE>
const MeRiBuRetrieveStatus
MessageRingBuffer::pop(char (&buffer)[SIZE], const bool drop)
{
  const MeRiBuRetrieveStatus ret = this->pop(buffer, SIZE - 2, drop);
  buffer[SIZE - 1] = 0x00;
  return ret;
}

const MeRiBuRetrieveStatus
MessageRingBuffer::pop(uint8_t buffer[], const size_t len, const bool drop)
{
  MRBDSN("Request to pop into a buffer of size ", len);
  if (this->isEmpty()) {
    MRBDS("RingBuffer is empty. Cannot pop()");
    return MeRiBuRetrieveStatus::RingBufferIsEmpty;
  }
  const size_t headSize = this->headPayloadSize();
  uint8_t crc;
  crc8_maxim_init(crc);
  for (size_t i = 0; i < headSize; i++) {
    if (i >= len) {
      MRBDSNSN("WARNING: Cannot put all ",
               headSize,
               " packet bytes into too small output buffer of size ",
               len);
      return MeRiBuRetrieveStatus::OutputBufferTooSmall;
    }
    buffer[i] = this->read(this->packetPayloadAddr(this->headAddr, i));
    crc8_maxim_update(crc, buffer[i]);
  }
  MRBDS("Read written checksum");
  uint8_t crcOnBuf =
    this->read(this->wrapBufAddr(this->headAddr, MeRiBuPackOffset(Checksum)));
  if (crcOnBuf == crc) {
    if (drop)
      this->dropHeadPacket();
    else
      MRBDS("Keeping HEAD packet");
    return MeRiBuRetrieveStatus::Ok;
  } else {
    MRBDSHSH("WARNING: Calculated checksum ",
             crc,
             " does not match written packet checksum ",
             crcOnBuf);
    return MeRiBuRetrieveStatus::InvalidChecksum;
  }
}

const bool
MessageRingBuffer::dropHeadPacket(void)
{
  MRBDS("Drop HEAD packet");
  if (this->isEmpty()) {
    MRBDS("Buffer is empty. No need to drop HEAD packet");
    return false;
  } else {
    bufaddr_t newAddr = this->previousPacketAddr(this->headAddr);
    if (this->cleanUpOnDrop) {
      const bufaddr_t headEndAddr = this->wrapBufAddr(
        this->headAddr,
        this->headPayloadSize() + MeRiBuPackOffset(PacketMetaDataSize));
      MRBDSH("Cleaning up HEAD packet with ", this->cleanUpValue);
      this->writeBufferRegion(this->headAddr, headEndAddr, this->cleanUpValue);
    }
    MRBDSHSH("Moving HEAD address from ", this->headAddr, " to ", newAddr)
    this->setHeadAddr(newAddr);
    this->setPacketCount(this->packetCount - 1);
    return true;
  }
}

// move the TAIL address forward to the next packet address

const bool
MessageRingBuffer::dropTailPacket(void)
{
  MRBDS("Drop TAIL packet");
  if (this->isEmpty()) {
    MRBDS("Buffer is empty. No need to drop TAIL packet");
    return false;
  } else {
    bufaddr_t newAddr = this->nextPacketAddr(this->tailAddr);
    if (this->cleanUpOnDrop) {
      const bufaddr_t tailEndAddr = this->wrapBufAddr(
        this->tailAddr,
        this->tailPayloadSize() + MeRiBuPackOffset(PacketMetaDataSize));
      MRBDSH("Cleaning up TAIL packet with ", this->cleanUpValue);
      this->writeBufferRegion(this->tailAddr, tailEndAddr, this->cleanUpValue);
    }
    MRBDSHSH("Moving TAIL address from ", this->tailAddr, " to ", newAddr)
    this->setTailAddr(newAddr);
    this->setPacketCount(this->packetCount - 1);
    return true;
  }
}
