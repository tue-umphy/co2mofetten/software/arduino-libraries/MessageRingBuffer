#include <Arduino.h>

const unsigned long
MessageRingBuffer::getReadCount(void)
{
  return this->readCount;
}

const unsigned long
MessageRingBuffer::getWriteCount(void)
{
  return this->writeCount;
}

void
MessageRingBuffer::resetAccessCounters(void)
{
  this->readCount = 0;
  this->writeCount = 0;
}

const bufaddr_t
MessageRingBuffer::getStartAddrOfSection(void)
{
  return this->startAddrOfSection;
}

const bufaddr_t
MessageRingBuffer::getEndAddrOfSection(void)
{
  return this->endAddrOfSection;
}

const bufaddr_t
MessageRingBuffer::getHeadAddr(void)
{
  twobyte_t addr;
  addr.byte[1] = this->read(this->secAddr(MeRiBuSecOffset(HeadAddrHigh)));
  addr.byte[0] = this->read(this->secAddr(MeRiBuSecOffset(HeadAddrLow)));
  this->headAddr = addr.word;
  return this->headAddr;
}

const bufaddr_t
MessageRingBuffer::getTailAddr(void)
{
  twobyte_t addr;
  addr.byte[1] = this->read(this->secAddr(MeRiBuSecOffset(TailAddrHigh)));
  addr.byte[0] = this->read(this->secAddr(MeRiBuSecOffset(TailAddrLow)));
  this->tailAddr = addr.word;
  return this->tailAddr;
}

void
MessageRingBuffer::setHeadAddr(bufaddr_t addr)
{
  union twobyte_t newaddr;
  newaddr.word = addr;
  MRBDSH("Setting HEAD address to ", addr);
  this->write(this->secAddr(MeRiBuSecOffset(HeadAddrHigh)), newaddr.byte[1]);
  this->write(this->secAddr(MeRiBuSecOffset(HeadAddrLow)), newaddr.byte[0]);
  this->headAddr = addr;
}

void
MessageRingBuffer::setTailAddr(bufaddr_t addr)
{
  union twobyte_t newaddr;
  newaddr.word = addr;
  MRBDSH("Setting TAIL address to ", addr);
  this->write(this->secAddr(MeRiBuSecOffset(TailAddrHigh)), newaddr.byte[1]);
  this->write(this->secAddr(MeRiBuSecOffset(TailAddrLow)), newaddr.byte[0]);
  this->tailAddr = addr;
}

const size_t
MessageRingBuffer::getPacketCount(void)
{
  twobyte_t count;
  count.byte[1] = this->read(this->secAddr(MeRiBuSecOffset(PacketCountHigh)));
  count.byte[0] = this->read(this->secAddr(MeRiBuSecOffset(PacketCountLow)));
  this->packetCount = count.word;
  return this->packetCount;
}

void
MessageRingBuffer::setPacketCount(const size_t count)
{
  MRBDSNS("Writing PacketCount (", count, ") to MeRiBu");
  twobyte_t newcount;
  newcount.word = count;
  this->write(this->secAddr(MeRiBuSecOffset(PacketCountHigh)),
              newcount.byte[1]);
  this->write(this->secAddr(MeRiBuSecOffset(PacketCountLow)),
              newcount.byte[0]);
  this->packetCount = count;
}
